class pesanan extends kue{
    public pesanan(String name, double price,double berat) {
        super(name, price);
        setBerat(berat);

    }
    private void setBerat(double berat){
        this.berat = berat;
        
    }
    private double berat;
    private double getBerat(){
        return berat;
    
    }
    public double hitungHarga(){
        return super.getPrice()*berat;
    }
    @Override
    public double Berat(){
        return berat;
    }
    @Override
    public double getPrice(){
        return super.getPrice();
    }
    @Override
    public void setPrice(double price){
        super.setPrice(price);
    }
    @Override
    public double Jumlah() {
        return 0;
    }
}

