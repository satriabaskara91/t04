public class Main {
    public static void main(String[] args) {
    kue cake[] = new kue[5];
    cake[0] = new pesanan ("Brownis",(int)(((Math.random()*(10-1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[1] = new pesanan("Bolu",(int)(((Math.random()*(10-1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[2] = new pesanan("Kue Sus",(int)(((Math.random()*(10-1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[3] = new stock("Kue Pisang",(int)(((Math.random()*(10 - 1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[4] = new stock("Onde-Onde",(int)(((Math.random()*(10 - 1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    for(kue jajan : cake){
        System.out.println(jajan.toString());
    }
    double totalSemua=0;
    for(kue jajan : cake){
        totalSemua += jajan.hitungHarga();
    }
    double totalHargaPesanan=0;
    for(int i = 0; i < 5; i++){
        totalHargaPesanan += cake[i].hitungHarga();
    }
    double totalBeratPesanan=0;
    for(int i = 0; i < 5; i++){
        totalBeratPesanan += cake[i].Berat();
    }
    double totalHargaJadi=0;
    for(int i = 3; i < 5; i++){
        totalHargaJadi += cake[i].hitungHarga();
    }
    double totalJumlahJadi=0;
    for(int i = 3; i < 5; i++){
        totalJumlahJadi += cake[i].Jumlah();
    }
    System.out.println("----------------------------------");
    System.out.println();
    System.out.println("Total Harga Kue\t\t:"+totalSemua);
    System.out.println("Total Harga Kue Pesanan\t:"+totalHargaPesanan);
    System.out.println("Total Berat Kue Pesanan\t:"+totalBeratPesanan);
    System.out.println("Total Harga Kue Jadi\t:"+totalHargaJadi);
    System.out.println("Total Jumlah Kue Jadi\t:"+totalJumlahJadi);
    double maks = 0;
    for(int i = 0; i < 5; i++){
        maks = Math.max(maks, cake[i].hitungHarga());
    }
    for(int i = 0; i < 5; i++){
        if (maks == cake[i].hitungHarga()){
            System.out.println("Kue Termahal\t\t"+cake[i].getName());
        }
    }
    }
}
